package com.example.customerrorhandling.ui

sealed class CustomException(
    message: String? = null
) : Exception(message) {
    object NoNetworkConnectionException : CustomException()
    object SpecificException : CustomException()

    fun mapToCustomError(): CustomError {
        return when (this) {
            is NoNetworkConnectionException -> {
                CustomError.NoNetworkConnection
            }
            is SpecificException -> {
                CustomError.SpecificError
            }
            /*else -> {
                CustomError.GeneralError(this.message)
            }*/
        }
    }
}