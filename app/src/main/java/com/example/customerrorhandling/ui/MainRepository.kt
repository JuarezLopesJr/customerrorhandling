package com.example.customerrorhandling.ui

class MainRepository {
    suspend fun fetchData(throwException: Boolean = false): String {
        /* this line would be the check to the NetworkManager to check
            the current network status. The line below simulates No connection status */
        if (throwException) {
            throw CustomException.NoNetworkConnectionException
        }
        return "Custom no network error message"
    }
}