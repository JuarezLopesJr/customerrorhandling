package com.example.customerrorhandling.ui

sealed class CustomError {
    object NoNetworkConnection : CustomError()
    object SpecificError : CustomError()
    data class GeneralError(val message: String?) : CustomError()
}