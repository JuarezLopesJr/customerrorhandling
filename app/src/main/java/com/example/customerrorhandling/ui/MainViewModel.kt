package com.example.customerrorhandling.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val mainRepository = MainRepository()

    var mainError by mutableStateOf<CustomError?>(CustomError.SpecificError)
        private set

    fun getData() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val result = mainRepository.fetchData(throwException = true)
            } catch (e: CustomException) {
                mainError = e.mapToCustomError()
            } catch (e: Exception) {
                mainError = CustomError.GeneralError(e.localizedMessage)
            }
        }
    }
}