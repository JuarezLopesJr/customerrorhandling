package com.example.customerrorhandling

import android.os.Bundle
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import com.example.customerrorhandling.ui.CustomError
import com.example.customerrorhandling.ui.MainViewModel
import com.example.customerrorhandling.ui.theme.CustomErrorHandlingTheme

class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
            WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
        )

        setContent {
            CustomErrorHandlingTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val errorState = mainViewModel.mainError

                    if (errorState != null) {
                        when (errorState) {
                            CustomError.SpecificError -> {
                                Text(text = "specific error message here")
                            }
                            CustomError.NoNetworkConnection -> {
                                Text(text = "no connection error message here")
                            }
                            is CustomError.GeneralError -> {
                                Text(text = "unspecified error message here")
                            }
                        }
                    }
                }
            }
        }
    }
}